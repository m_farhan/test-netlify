# selloffvacations.com/

## 🚀 Quick start

### **Installation**

**🚨🚨🚨 BEFORE YOU START 🚨🚨🚨**

> If you have never setup your global `.npmrc` to have access to Sunwing's private npm registry, you need to do that first before running `npm install`
>
> Please refer to the registry [documentation](https://dev.azure.com/SunwingTravelGroup/AtCore%20Project/_packaging?_a=connect&feed=sunwing-shared-components) for setting up the configuration to the private npm registry.

Running `npm install` in the root directory will install everything you need for development.

Included in the project is a `.env.example` file and a `.env.preview.example` file, you will need to update the files with the correct environment variables from Contentful and remove the `.example` in the file name.

Once updated, we can run `npm start` for the first time! The very first time will take a few minutes to retrieve and cache the data from Contentful. Subsequent runs will be much faster as it will only retrieve the changes since the last retrieval.

Once started, you will be prompted with:

> You can now view sunwing in the browser.
>
> http://localhost:8000/
>
> View GraphiQL, an in-browser IDE, to explore your site's data and schema
>
> http://localhost:8000/\_\_\_graphql

And we're off 😀!

> ⚠️ In order to increase predictability and consistencies between local environments and Netlify, please use **Node 12.18.0** for the smoothest experience.
>
> It is recommended to use a Node Version Manager such as [nvm](https://github.com/nvm-sh/nvm) for Mac or [nvm-windows](https://github.com/coreybutler/nvm-windows) in order to manage multiple versions of Node on the same machine.

## **Development workflow**

### Isolated Component Development

[Storybook](https://storybook.js.org/) is used as an isolated environment to develop and document reusable components. We leverage several addons which extends Storybook to assist with accessibility and responsive testing as well as improve the documentation process with interactive demos.

### Code Linting

[ESLint](https://eslint.org/) and [stylelint](https://stylelint.io/) are used to enforce our coding standards as well as catch common errors in the code base. During development, they are automatically running in the background and will output warnings and errors in the console. Please note that any errors that are not fixable automatically will block your commit.

For the best development experience (if using VS Code), download the [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) and [stylelint](https://marketplace.visualstudio.com/items?itemName=stylelint.vscode-stylelint) extensions.

For most simple errors, these two extensions should automatically fix them on save. If you are not using them, or they aren't auto-fixing them, you can run `npm run lint:fix` or `npm run lint:js:fix` and `npm run lint:css:fix` to auto-fix these issues.

### Icons

Generates all icons related assets (font, scss, json)

Make sure you have `gulp-cli` installed on your system.
If not run this first: `npm install --global gulp-cli`

### `npm run build:icons`

### Formatting

[Prettier](https://prettier.io/) is used to format, enforce standardization of the code base and has been configured to reduce friction when merging and during pull requests. By automatically formatting the code base before a commit, it allows the whole team to have the same code style which will remove those pesky commits where with 1 line of change becoming a whole file of changes because one developer uses spaces and another uses tabs.

For the best development experience (if using VS Code), download the [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) extension.

### Testing

[Jest](https://jestjs.io/) is used as the testing framework using the [React Testing Library](https://testing-library.com/docs/react-testing-library/intro) utilities library for this project.

### Pre-commit

[Husky](https://github.com/typicode/husky) is used alongside with [lint-staged](https://github.com/okonet/lint-staged) by leveraging Git hooks to run code linting, formatting and running related tests before each commit. It is part of this project and does not require any additional extensions or setup.

When you commit, it will lint and try to fix all issues before actually committing the changes. If there are unresolvable issues or failed test cases, it will block the commit until they are fixed.

## 🛠Crucial Commands

Although there are many scripts in this repo, the most important ones are the ones used to develop and test. Many of the scripts included will auto-execute during the development, build and commit process. You can find the most important commands below.

### `npm start` || `npm run develop`

Run in the project locally using the Contentful Delivery API (published posts only).

### `npm run develop:preview`

Run in the project locally using the Contentful Preview API (published and draft posts).

### `npm run build`

Run a production build into `./public`. The result is what will be published to Netlify. You can run `npm run serve` to test out the build.

### `npm run storybook`

Run Storybook locally for developing and writing documentation for reusable UI components in isolation.

### `npm run build-storybook`

Run a production build of Storybook into `./.docs`. The result is what will be published to Netlify and referenced as documentation.

### `npm run lint` and `npm run lint:fix`

Although ESLint and stylelint are running during development (`npm run lint`) and pre-commit (`npm run lint:fix`), these two scripts and the following are available for checking and fixing common errors and styling to fit our coding standards:

- `npm run lint:js`
- `npm run lint:js:fix`
- `npm run lint:css`
- `npm run lint:css:fix`

### `npm run test`

Run all the tests in the project.

### `npm run test:staged`

Runs all the related tests based on changed files.

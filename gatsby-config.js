/* eslint-disable import/no-extraneous-dependencies */
const autoprefixer = require("autoprefixer");

require("dotenv").config({
	path: `.env${process.env.PREVIEW ? ".preview" : ""}`,
});
console.warn("process.env.INCOMING_HOOK_BODY");
console.warn(process.env.INCOMING_HOOK_BODY);
const contentfulConfig = {
	spaceId: process.env.CONTENTFUL_SPACE_ID,
	accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
	environment: process.env.CONTENTFUL_ENVIRONMENT || "master",
	richText: {
		resolveFieldLocales: true,
	},
	useNameForId: false,
};

const contentfulRclConfig = {
	spaceId: process.env.CONTENTFUL_RCL_SPACE_ID,
	accessToken: process.env.CONTENTFUL_RCL_ACCESS_TOKEN,
	environment: process.env.CONTENTFUL_RCL_ENVIRONMENT || "master",
};

if (process.env.PREVIEW) {
	contentfulConfig.host = process.env.CONTENTFUL_HOST || "cdn.contentful.com";
}

const { spaceId, accessToken } = contentfulConfig;

if (!spaceId || !accessToken) {
	throw new Error("Contentful spaceId and the access token need to be provided.");
}

module.exports = {
	flags: {
		PRESERVE_WEBPACK_CACHE: true,
		FAST_DEV: true,
		PARALLEL_SOURCING: true,
	},
	siteMetadata: {
		siteUrl: process.env.CONTEXT === "production" ? process.env.URL : process.env.DEPLOY_PRIME_URL,
	},
	plugins: [
		{
			resolve: "gatsby-plugin-schema-snapshot",
			options: {
				include: {
					plugins: ["gatsby-source-contentful-extended"],
				},
			},
		},
		"gatsby-plugin-sharp",
		"gatsby-plugin-react-helmet",
		// HACK: gatsby-source-contentful-extended leverages gatsby-source-contentful
		// When upgrading gatsby-source-contentful - update gatsby-source-contentful-extended as well!
		{
			resolve: "gatsby-source-contentful-extended",
			options: contentfulConfig,
		},
		{
			resolve: "gatsby-source-contentful-extended",
			options: contentfulRclConfig,
		},
		{
			resolve: "gatsby-plugin-eslint",
			options: {
				stages: ["develop"],
			},
		},
		{
			resolve: "gatsby-plugin-sass",
			options: {
				implementation: require("node-sass"),
				cssLoaderOptions: {
					camelCase: false,
				},
				additionalData: "@import 'src/styles/global.scss';",
				sassOptions: {
					includePaths: ["src/styles"],
				},
				postCssPlugins: [autoprefixer({ grid: true })],
			},
		},
		{
			resolve: "gatsby-plugin-netlify",
			options: {
				headers: {
					"/*": ["X-XSS-Protection: 1; mode=block", "X-Content-Type-Options: nosniff"],
				},
				mergeSecurityHeaders: false,
				mergeCachingHeaders: false,
				mergeLinkHeaders: false,
				generateMatchPathRewrites: false,
			},
		},
		"gatsby-plugin-remove-fingerprints",
		{
			resolve: "gatsby-plugin-google-tagmanager",
			options: {
				id: process.env.GTM_ID,
				includeInDevelopment: false,
			},
		},
	],
};

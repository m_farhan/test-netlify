const path = require("path");
const dayjs = require("dayjs");
const utils = require("./src/utils/gatsby-helpers");

const SITE_LOCALES = ["en-CA", "fr-CA"];
const TODAY = dayjs().format("YYYY-MM-DD");

exports.createPages = ({ graphql, actions }) => {
	const { createPage, createRedirect } = actions;
	const TEMPLATES = {
		FLUID_PAGE: "fluidLayoutPages",
		HOME_PAGE: "homepage",
		ALERTS_PAGE: "alerts",
	};

	const addSEOSupportToURL = oldURL => {
		const newURL = utils.addSEOSupportToURL(oldURL);
		const cleanOldURL = oldURL.trim().toLowerCase();
		if (cleanOldURL !== newURL) {
			createRedirect({
				fromPath: encodeURI(cleanOldURL),
				toPath: newURL,
				isPermanent: true,
			});
		}
		return newURL;
	};

	return graphql(
		`
			query loadAllPages {
				fluidLayoutPages: allContentfulFluidLayout {
					edges {
						node {
							title
							slug
							id
							contentful_id
							node_locale
							seo {
								robots
							}
							pageSections {
								pageSections {
									... on ContentfulPromotionRule {
										rule
									}
									... on ContentfulComponentWrapper {
										wrapperSections {
											pageSections {
												... on ContentfulPromotionRule {
													rule
												}
											}
										}
									}
								}
							}
						}
					}
				}
				destinations: allContentfulDestination {
					edges {
						node {
							id
							contentful_id
							destinationCode
							node_locale
							seo {
								robots
							}
							destinationName
							urlMappingSlug
							pageSections {
								pageSections {
									... on ContentfulPromotionRule {
										rule
									}
									... on ContentfulComponentWrapper {
										wrapperSections {
											pageSections {
												... on ContentfulPromotionRule {
													rule
												}
											}
										}
									}
								}
							}
							destinationCountry {
								title
								urlMappingSlug
								excursionCode
							}
						}
					}
				}
				countries: allContentfulCountry {
					edges {
						node {
							id
							contentful_id
							node_locale
							seo {
								robots
							}
							urlMappingSlug
							countryCode
							excursionCode
							pageSections {
								pageSections {
									... on ContentfulPromotionRule {
										rule
									}
									... on ContentfulComponentWrapper {
										wrapperSections {
											pageSections {
												... on ContentfulPromotionRule {
													rule
												}
											}
										}
									}
								}
							}
						}
					}
				}
				hardCodedComponentRoutes: allContentfulHardCodedComponent(
					filter: { routes: { ne: null } }
				) {
					edges {
						node {
							title
							baseUrl
							routes
							node_locale
						}
					}
				}
			}
		`
	).then(
		({ data: { fluidLayoutPages, destinations, countries, hardCodedComponentRoutes }, errors }) => {
			if (errors) {
				throw errors;
			}
			const hardCodedComponentsRouting = hardCodedComponentRoutes.edges.map(edge => ({
				baseUrl: edge.node.baseUrl,
				routes: edge.node.routes,
				node_locale: edge.node.node_locale,
			}));

			const createAdModuleGatewayPages = ({ node, component, context }) => {
				if (node.pageSections && node.pageSections.pageSections) {
					let adModuleSections = false;
					node.pageSections.pageSections.forEach(i => {
						if (i.rule) adModuleSections = true;
						else if (i.wrapperSections) {
							if (i.wrapperSections.pageSections) {
								i.wrapperSections.pageSections.forEach(j => {
									if (j.rule) adModuleSections = true;
								});
							}
						}
					});
					/*
				if (adModuleSections) {
					gateways.forEach(gateway => {
						createPage({
							path: utils.addSEOSupportToURL(`${context.basePath}/from-${gateway.Name}`),
							component: path.resolve(component),
							context: {
								...context,
								seo: { robots: ["noindex", "nofollow"] },
								ctf_id: `${node.contentful_id}-${gateway.Code}`,
								gateway,
							},
						});
						if (gateway.Code === "YQB") {
							createPage({
								path: utils.addSEOSupportToURL(`${context.basePath}/from-quebec`),
								component: path.resolve(component),
								context: {
									...context,
									seo: { robots: ["noindex", "nofollow"] },
									ctf_id: `${node.contentful_id}-${gateway.Code}-quebec`,
									gateway,
								},
							});
						}
					});
				} */
				}
			};

			// Fluid Page Layouts
			fluidLayoutPages.edges.forEach(({ node }) => {
				const lang = node.node_locale.slice(0, 2);
				let slug = node.title;

				if (node.slug) {
					slug = node.slug;
				}
				const dynamicRouting = hardCodedComponentsRouting.find(
					routing => routing.baseUrl === slug && routing.node_locale === node.node_locale
				);

				if (slug === "homepage") {
					const page = {
						path: `/${lang}/`,
						component: path.resolve("src/templates/home.js"),
						context: {
							id: node.id,
							ctf_id: node.contentful_id,
							locale: node.node_locale,
							seo: node.seo,
							date: TODAY,
							template: TEMPLATES.HOME_PAGE,
						},
					};
					createPage(page);
				} else if (dynamicRouting) {
					const page = {
						path: addSEOSupportToURL(`/${lang}/${slug}`),
						component: path.resolve("src/templates/fluid.js"),
						context: {
							id: node.id,
							ctf_id: node.contentful_id,
							locale: node.node_locale,
							seo: node.seo,
							date: TODAY,
							template: TEMPLATES.FLUID_PAGE,
							basePath: utils.addSEOSupportToURL(`/${lang}/${slug}`),
						},
					};
					createPage(page);
					createAdModuleGatewayPages({
						node,
						...page,
					});

					dynamicRouting.routes.forEach((route, index) => {
						const dynamicPage = {
							path: addSEOSupportToURL(`/${lang}/${slug}/${route}`),
							component: path.resolve("src/templates/fluid.js"),
							context: {
								id: node.id,
								ctf_id: `${node.contentful_id}-${index}`,
								locale: node.node_locale,
								seo: { robots: ["noindex", "nofollow"] },
								date: TODAY,
								template: TEMPLATES.FLUID_PAGE,
								basePath: utils.addSEOSupportToURL(`/${lang}/${slug}/${route}`),
							},
						};
						createPage(dynamicPage);
						createAdModuleGatewayPages({
							node,
							...dynamicPage,
						});
					});
				} else {
					const page = {
						path: addSEOSupportToURL(`/${lang}/${slug}`),
						component: path.resolve("src/templates/fluid.js"),
						context: {
							id: node.id,
							ctf_id: node.contentful_id,
							locale: node.node_locale,
							seo: node.seo,
							date: TODAY,
							template: TEMPLATES.FLUID_PAGE,
							basePath: utils.addSEOSupportToURL(`/${lang}/${slug}`),
						},
					};
					createPage(page);
					createAdModuleGatewayPages({
						node,
						...page,
					});
				}
			});

			// Destination Pages
			// TODO: Util for sanitizing urls?
			// TODO: Get logic for {from} route - where does the list come from?
			// TODO: Finalize logic for {video} - do we redirect old links to query?
			// /{culture}/destinations/{country}/{destination}/{from}/{video}
			// Cache globally used destination slugs (city + country)
			const destinationMapping = utils.createDestinationMapping(destinations.edges);

			SITE_LOCALES.forEach(locale => {
				const lang = locale.slice(0, 2);

				// Sub-pages /{country}/{destination}/{from}
				destinationMapping[lang].forEach((_destinations, _country) => {
					// Top level country page
					const countryData = countries.edges.find(
						({ node }) => node.urlMappingSlug === _country && node.node_locale === locale
					);

					let countryCode = "";

					if (countryData.node && countryData.node.countryCode) {
						countryCode = countryData.node.countryCode;
					}
					let excursionCode = "";

					if (countryData.node && countryData.node.excursionCode) {
						excursionCode = countryData.node.excursionCode;
					}

					const cPage = {
						path: addSEOSupportToURL(`/${lang}/destinations/${_country}`),
						component: path.resolve("src/templates/destinations/country.js"),
						context: {
							id: countryData.node.id,
							ctf_id: `${countryData.node.contentful_id}-country`,
							locale,
							seo: countryData.node.seo,
							date: TODAY,
							template: TEMPLATES.DESTINATION_PAGE,
							countryCode,
							excursionCode,
							basePath: utils.addSEOSupportToURL(`/${lang}/destinations/${_country}`),
						},
					};
					createPage(cPage);
					createAdModuleGatewayPages({
						node: countryData.node,
						...cPage,
					});
					_destinations.forEach(destination => {
						if (_country === destination.slug) {
							const dcPage = {
								path: addSEOSupportToURL(`/${lang}/destinations/${destination.slug}`),
								component: path.resolve("src/templates/destinations/destination.js"),
								context: {
									id: destination.id,
									ctf_id: `${destination.contentful_id}-destination-top`,
									locale,
									seo: destination.seo,
									localeRegex: `/${lang}/`, // For excursions that has fr and en-CA as local
									date: TODAY,
									template: TEMPLATES.DESTINATION_PAGE,
									destination: destination.destination,
									destinationRegex: `/${destination.destinationCode}/`,
									country: destination.country,
									destinationSlug: destination.slug,
									basePath: utils.addSEOSupportToURL(`/${lang}/destinations/${destination.slug}`),
								},
							};
							createPage(dcPage);
							createAdModuleGatewayPages({
								node: destination,
								...dcPage,
							});
						}

						const dPage = {
							path: addSEOSupportToURL(`/${lang}/destinations/${_country}/${destination.slug}`),
							component: path.resolve("src/templates/destinations/destination.js"),
							context: {
								id: destination.id,
								ctf_id: `${destination.contentful_id}-destination`,
								locale,
								seo: destination.seo,
								localeRegex: `/${lang}/`, // For excursions that has fr and en-CA as local
								date: TODAY,
								template: TEMPLATES.DESTINATION_PAGE,
								destinationRegex: `/${destination.destinationCode}/`,
								destination: destination.destination,
								country: destination.country,
								destinationSlug: destination.slug,
								basePath: utils.addSEOSupportToURL(
									`/${lang}/destinations/${_country}/${destination.slug}`
								),
							},
						};
						createPage(dPage);
						createAdModuleGatewayPages({
							node: destination,
							...dPage,
						});
					});
				});
			});

			// 404 redirects
			SITE_LOCALES.forEach(locale => {
				const lang = locale.slice(0, 2);

				createRedirect({
					fromPath: `/${lang}/*`,
					toPath: `/${lang}/404/`,
					statusCode: "404",
				});
			});
		}
	);
};

exports.onCreatePage = async ({ page, actions }) => {
	const { createPage, deletePage } = actions;
	// Check if the page is a localized 404
	if (page.path.match(/^(\/[a-z]{2})?\/404\/$/)) {
		const oldPage = { ...page };
		// Get the language code from the path, and match all paths
		// starting with this code (apart from other valid paths)
		const langCode = page.path.split("/")[1];
		// Recreate the modified page
		deletePage(oldPage);

		// Non-localized default
		if (langCode === "404") {
			createPage({ ...page, context: { date: TODAY } });
		} else {
			createPage({
				...page,
				matchPath: `/${langCode}/*`,
				context: {
					seo: { robots: ["noindex", "nofollow"] },
					date: TODAY,
				},
			});
		}
	}
};

/* eslint-disable no-console */
// HACK: TEMPORARY SOLUTION TO ENABLE PARALLEL SOURCING
// We import and re-export the `gatsby-source-contentful` plugin from here
// Inside of our own sourceNodes export, we leverage the contentful plugin
// to be able to run the retrieve ad module data after we get the data from contentful
const {
	setFieldsOnGraphQLNodeType,
	onPreInit,
	sourceNodes: contentfulSourceNodes,
	onPreExtractQueries,
	// HACK: gatsby-source-contentful is not installed here intentionally to let npm source
	// upwards into the project's node_modules to ensure we have the same version
	// eslint-disable-next-line import/no-extraneous-dependencies
} = require("gatsby-source-contentful/gatsby-node");
const fetch = require("node-fetch");

exports.setFieldsOnGraphQLNodeType = setFieldsOnGraphQLNodeType;
exports.onPreInit = onPreInit;
exports.onPreExtractQueries = onPreExtractQueries;

const isDevMode = process.env.AD_MODULES_DEV_MODE === "true";
const devCache = new Map();

const fetchRuleGroupPromos = async (ruleGroup, lang, retry = 0) => {
	let group = [];
	try {
		group = await fetch(
			`https://infoservice.sunwingtravelgroup.com/search/Offers/Promotions?lang=${lang}&type=flight&alias=btd&name=${encodeURIComponent(
				ruleGroup.rule.trim()
			)}`
		).then(response => {
			if (response.ok) {
				return response.json();
			}
			if (retry < 3) {
				return fetchRuleGroupPromos(ruleGroup, lang, retry + 1);
			}
			throw response;
		});
	} catch (error) {
		if (retry < 3) {
			return fetchRuleGroupPromos(ruleGroup, lang, retry + 1);
		}
		console.warn(
			`Error fetching rule group promos (${ruleGroup.rule} - ${lang} - ${retry}): `,
			error
		);
		group = [{ errors: { rule: ruleGroup.rule, lang, error } }];
	}

	return group;
};

const fetchRuleGroup = async ruleGroup => {
	try {
		const lang = ruleGroup.node_locale.substring(0, 2).toLowerCase().includes("fr") ? "fr" : "en";
		let ruleGroupData = [];

		if (isDevMode && devCache.has(lang)) {
			const cachedRule = devCache.get(lang);
			ruleGroupData = cachedRule.data;
			console.warn(`[Dev Mode - One Rule] ${ruleGroup.rule} is using ${cachedRule.rule}`);
		} else {
			ruleGroupData = await fetchRuleGroupPromos(ruleGroup, lang);
			if (isDevMode) {
				devCache.set(lang, {
					rule: ruleGroup.rule,
					data: ruleGroupData,
				});
			}
			console.log(`${ruleGroup.rule} with ${ruleGroupData.length} gateways`);
		}
		if (ruleGroupData.length === 0) {
			console.warn(ruleGroupData);
		}
		const allErrors = ruleGroupData
			.reduce((prev, curr) => {
				if (curr) {
					prev.push(curr.errors);
				}
				return prev;
			}, [])
			.filter(x => x);

		return {
			ruleGroup,
			data: ruleGroupData,
			errors: allErrors.length ? allErrors : null,
		};
	} catch (err) {
		console.warn("FetchRuleGroup", err);

		return {
			ruleGroup,
			data: [],
			errors: [err],
			totalAPICalls: 0,
		};
	}
};

function fetchAllRuleGroups(items, chunkSize = 5) {
	const _chunks = [];

	for (let i = 0; i < items.length; i += chunkSize) {
		_chunks.push(items.slice(i, i + chunkSize));
	}

	const allRuleGroupData = _chunks.reduce(async (acc, chunk) => {
		const promisedAccumulator = await acc;

		const promises = chunk.map(ruleGroup => fetchRuleGroup(ruleGroup));
		const ruleGroupData = await Promise.all(promises);

		ruleGroupData.forEach(({ data, ruleGroup, errors, totalAPICalls }) => {
			promisedAccumulator.data.push({
				data,
				ruleGroup,
			});

			if (errors) {
				promisedAccumulator.errors.push({
					errors,
					ruleGroup,
				});
			}
			promisedAccumulator.totalAPICalls += totalAPICalls;
		});

		return promisedAccumulator;
	}, Promise.resolve({ data: [], errors: [], totalAPICalls: 0 }));

	return allRuleGroupData;
}

exports.sourceNodes = async (sdk, pluginOptions) => {
	await contentfulSourceNodes(sdk, pluginOptions);

	if (pluginOptions.spaceId === process.env.CONTENTFUL_SPACE_ID) {
		const spaceInfo = `${pluginOptions.spaceId}-${pluginOptions.environment}`;
		const { createNodeField } = sdk.actions;
		const allPromotionRules = sdk.getNodesByType("ContentfulPromotionRule");

		console.log(`Starting to fetch data from AdModule - ${spaceInfo}`);
		console.log(`Fetching AdModule data for ${allPromotionRules.length} rule groups`);
		console.time(`Fetch AdModule data - ${spaceInfo}`);

		let allRuleGroupData = {};

		if (isDevMode) {
			console.warn("[Dev Mode - One Rule]");
			allRuleGroupData = await fetchAllRuleGroups(allPromotionRules, 1);
		} else {
			allRuleGroupData = await fetchAllRuleGroups(allPromotionRules, 20);
		}

		// Add Rule Group data to PromotionRule
		allRuleGroupData.data.forEach(ruleGroupData => {
			createNodeField({
				node: ruleGroupData.ruleGroup,
				name: "admodule",
				value: ruleGroupData.data,
			});
		});

		if (allRuleGroupData.errors.length) {
			console.error(
				`Unable to fetch ${allRuleGroupData.errors.length} rules: `,
				JSON.stringify(
					allRuleGroupData.errors.map(i => `${i.errors[0].rule} [${i.errors[0].lang}]`).join(", ")
				)
			);
		}
		console.timeEnd(`Fetch AdModule data - ${spaceInfo}`);
	}
};

/* eslint-disable react/prop-types */
import React from "react";
import "./Colors.scss";

const primaryColors = [
	"inherit",
	"current",
	"sun",
	"dune",
	"stone",
	"tide",
	"rock",
	"green",
	"water",
	"pebble",
	"sand",
	"sky",
	"pearl",
	"sea-shell",
	"star-fish",
	"coral",
	"coral-reef",
	"royal",
	"sea",
	"turquoise",
	"beach",
	"wedding",
	"shadow",
];
const systemColors = [
	"error",
	"error-light",
	"success",
	"success-light",
	"warning",
	"warning-light",
];
const socialColors = ["facebook", "twitter", "pinterest", "youtube"];

const Box = ({ color }) => (
	<div className="color__box">
		<div className={`color__box__sq bg--${color}`}>bg--{color}</div>
		<span className={`color__box__legend text--${color}`}>text--{color}</span>
	</div>
);

const Colors = () => (
	<div className="color-root">
		<div className="color-row" data-theme="primary">
			<h2 className="color-row__title">Primary</h2>
			{primaryColors.map(color => (
				<Box color={color} />
			))}
		</div>
		<div className="color-row">
			<h2 className="color-row__title">System</h2>
			{systemColors.map(color => (
				<Box color={color} />
			))}
		</div>
		<div className="color-row">
			<h2 className="color-row__title">Social</h2>
			{socialColors.map(color => (
				<Box color={color} />
			))}
		</div>
	</div>
);

export default Colors;
export { Colors, primaryColors, systemColors };

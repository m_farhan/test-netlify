import { graphql } from "gatsby";

export default null;

export const componentWrapperQuery = graphql`
	fragment componentWrapper on ContentfulComponentWrapper {
		id
		title
		titleAs
		titleSize
		titleAlignment
		titleId
		displayTitle
		fullWidth
		padding
		backgroundColor
		theme
		backgroundImage {
			url
			secure_url
			width
			height
			raw_transformation
			version
			public_id
			format
		}
		wrapperSections {
			...fluidSections
		}
	}
`;

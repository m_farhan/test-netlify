import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames/bind";
import { Wrapper } from "../../sharedComponents/Wrapper";
import { Heading } from "../Headings/Heading";

import * as styles from "./ComponentWrapper.module.scss";

const cx = classNames.bind(styles);

const ComponentWrapper = ({ title, fullWidth, backgroundColor, children }) => {
	let wrapperBackgroundClass = null;

	if (backgroundColor) {
		wrapperBackgroundClass = backgroundColor;
	}

	let fulWidthClass = null;

	if (fullWidth) {
		fulWidthClass = "fullWidth";
	}

	return (
		<div className={cx("containerWrapper", wrapperBackgroundClass, fulWidthClass)}>
			<Wrapper padding container>
				{title && (
					<Heading as="h2" size="h1" center>
						{/* TODO:   THIS should be dynamic */}
						{title}
					</Heading>
				)}
				{children}
			</Wrapper>
		</div>
	);
};

ComponentWrapper.propTypes = {
	title: PropTypes.string,
	fullWidth: PropTypes.bool,
	backgroundColor: PropTypes.string,
	children: PropTypes.node.isRequired,
};

ComponentWrapper.defaultProps = {
	title: null,
	fullWidth: false,
	backgroundColor: null,
};

export default ComponentWrapper;
export { ComponentWrapper };

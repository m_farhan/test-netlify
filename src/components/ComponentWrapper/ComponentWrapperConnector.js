import React from "react";
import PropTypes from "prop-types";
import {
	Section,
	SectionTitleAlign,
	SectionTheme,
	SectionPadding,
} from "../../sharedComponents/Section/Section";

const ComponentWrapperConnector = ({ data, useWrapper, children }) =>
	useWrapper ? (
		<Section
			title={data?.displayTitle}
			titleAs={data?.titleAs || "h2"}
			titleSize={data?.titleSize || "h1"}
			titleAlignment={data?.titleAlignment || "center"}
			titleId={data?.titleId ? data?.titleId : undefined}
			padding={data?.padding ? data?.padding : undefined}
			theme={data?.theme || data?.backgroundColor === "pearl" ? "dark" : null}
		>
			{children}
		</Section>
	) : (
		children
	);

ComponentWrapperConnector.propTypes = {
	data: PropTypes.shape({
		displayTitle: PropTypes.string,
		titleAs: PropTypes.string,
		titleSize: PropTypes.string,
		titleAlignment: PropTypes.oneOf([...SectionTitleAlign]),
		titleId: PropTypes.string,
		theme: PropTypes.oneOf([...SectionTheme]),
		padding: PropTypes.oneOf([...SectionPadding]),
		backgroundColor: PropTypes.oneOf(["pearl", "dark"]),
		backgroundImage: PropTypes.arrayOf(
			PropTypes.shape({
				secure_url: PropTypes.string,
				width: PropTypes.string,
				height: PropTypes.string,
				raw_transformation: PropTypes.string,
				version: PropTypes.string,
				public_id: PropTypes.string,
				format: PropTypes.string,
			})
		),
		wrapperSections: PropTypes.object,
	}),
	children: PropTypes.node.isRequired,
	useWrapper: PropTypes.bool,
};

ComponentWrapperConnector.defaultProps = {
	data: null,
	useWrapper: true,
};

export default ComponentWrapperConnector;
export { ComponentWrapperConnector };

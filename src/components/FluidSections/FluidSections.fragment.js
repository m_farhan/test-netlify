import { graphql } from "gatsby";

export default null;

export const sharedPageSectionsQuery = graphql`
	fragment sharedPageSections on ContentfulFluidPageSections {
		pageSections {
			__typename
			... on ContentfulParagraphSection {
				sys {
					contentType {
						sys {
							id
						}
					}
				}
				...ParagraphSection
			}
		}
	}
`;

export const pageSectionsQuery = graphql`
	fragment pageSections on ContentfulFluidPageSections {
		id
		contentful_id
		node_locale
		__typename
		title
		...sharedPageSections
	}
`;

export const fluidSectionsQuery = graphql`
	fragment fluidSections on ContentfulFluidPageSections {
		...sharedPageSections
	}
`;

/* eslint-disable no-unused-vars */
import React from "react";
import PropTypes from "prop-types";

// eslint-disable-next-line import/no-cycle
import { ComponentWrapperConnector } from "../ComponentWrapper";
// eslint-disable-next-line import/no-cycle
import { ParagraphSectionConnector } from "../ParagraphSection";

const FluidSections = ({ data, location, locale, useWrapper }) => (
	<>
		{data?.pageSections?.map(section => {
			const id = section?.sys?.contentType?.sys?.id;
			return (
				<ComponentWrapperConnector
					data={id === "componentWrapper" && section}
					locale={locale}
					useWrapper={useWrapper && id !== "whiteSpaceComponent" && id !== "componentBlock"}
				>
					{(() => {
						switch (id) {
							case "paragraphSection":
								return <ParagraphSectionConnector data={section} key={section.id} />;
							case "componentWrapper":
								return (
									<FluidSections
										useWrapper={false}
										data={section.wrapperSections}
										locale={locale}
									/>
								);
							default:
								return null;
						}
					})()}
				</ComponentWrapperConnector>
			);
		})}
	</>
);

export default FluidSections;
export { FluidSections };

/* Declare page prop */
FluidSections.propTypes = {
	data: PropTypes.object.isRequired,
	location: PropTypes.object,
	locale: PropTypes.string,
	useWrapper: PropTypes.bool,
};

FluidSections.defaultProps = {
	useWrapper: true,
	location: null,
	locale: null,
};

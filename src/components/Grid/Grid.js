import React from "react";
import { string, node, oneOfType, arrayOf, number } from "prop-types";
import classNames from "classnames/bind";
import styles from "./Grid.module.scss";

const cx = classNames.bind(styles);

export const Row = ({ cols, children, className }) => (
	<div
		className={cx("grid", {
			[styles[`grid--${cols}`]]: cols,
			[className]: className,
		})}>
		{children}
	</div>
);

export const Cell = ({ children, className }) => (
	<div className={cx("grid__item", className)}>{children}</div>
);

Row.propTypes = {
	children: oneOfType([arrayOf(node), node]).isRequired,
	cols: oneOfType([string, number]),
	className: string,
};

Row.defaultProps = {
	cols: null,
	className: "",
};

Cell.propTypes = {
	children: oneOfType([arrayOf(node), node]).isRequired,
	className: string,
};

Cell.defaultProps = {
	className: "",
};

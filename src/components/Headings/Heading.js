import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import * as styles from "./Heading.module.scss";

const HeadingType = ["h1", "h2", "h3", "h4", "h5"];
const HeadingSize = [...HeadingType, "h1alt", "h3alt", "base"];

const Heading = ({ weight, uppercase, as, narrow, size, className, children, id, center }) => {
	const Tag = `${as}`;
	const classes = cx({
		[styles[`heading--${size}`]]: size,
		[styles[`heading--${weight}`]]: weight,
		[styles[`heading--${center}`]]: center,
		[styles["heading--case"]]: uppercase,
		[styles["heading--narrow"]]: narrow,
		[className]: className,
	});
	return (
		<Tag className={classes} id={id}>
			{children}
		</Tag>
	);
};

export default Heading;
export { Heading, HeadingType, HeadingSize };

Heading.propTypes = {
	as: PropTypes.oneOf([...HeadingType]),
	weight: PropTypes.oneOf(["normal", "bold"]),
	size: PropTypes.oneOf([...HeadingSize]),
	uppercase: PropTypes.bool,
	children: PropTypes.node,
	narrow: PropTypes.bool,
	center: PropTypes.oneOf(["default", "center"]),
	id: PropTypes.string,
	className: PropTypes.string,
};

Heading.defaultProps = {
	as: "h1",
	weight: null,
	id: "",
	size: null,
	uppercase: false,
	children: "Dynamic Heading",
	narrow: false,
	center: false,
	className: "",
};

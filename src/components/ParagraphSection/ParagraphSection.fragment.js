import { graphql } from "gatsby";

export default null;

export const Paragraph = graphql`
	fragment Paragraph on ContentfulParagraph {
		id
		heading
		subHeading
		variant
		content {
			raw
			references {
				__typename
			}
		}
	}
`;
export const ParagraphSection = graphql`
	fragment ParagraphSection on ContentfulParagraphSection {
		id
		heading
		subHeading
		variant
		layout
		paragraphs {
			...Paragraph
		}
		otherDetails {
			raw
			references {
				__typename
			}
		}
	}
`;

import React from "react";
import classNames from "classnames/bind";
import { arrayOf, object, oneOf, string } from "prop-types";
import { ContentSection } from "../../sharedComponents/ContentSection";
// eslint-disable-next-line import/no-cycle
import { RichText } from "../RichText";
import * as styles from "./ParagraphSection.module.scss";

const cx = classNames.bind(styles);

const ParagraphSection = ({ heading, subHeading, paragraphs, variant, footer, layout }) => {
	const layoutClasses = layout || [];
	return (
		<>
			<ContentSection className={cx("section", variant, ...layoutClasses)}>
				{heading && (
					<ContentSection.Title className={cx("sectionTitle", variant, ...layoutClasses)}>
						{heading}
					</ContentSection.Title>
				)}
				{subHeading && (
					<ContentSection.SubTitle className={cx("sectionContent", variant, ...layoutClasses)}>
						{subHeading}
					</ContentSection.SubTitle>
				)}
				{paragraphs && paragraphs.length && (
					<ContentSection.Content>
						{paragraphs.map((p, index) => (
							<ContentSection
								// eslint-disable-next-line react/no-array-index-key
								key={index}
								className={cx("paragraph", p.variant, variant, ...layoutClasses)}
							>
								{p.heading && (
									<ContentSection.Title
										className={cx("paragraphTitle", p.variant, variant, ...layoutClasses)}
									>
										{p.heading}
									</ContentSection.Title>
								)}
								{p.subHeading && (
									<ContentSection.SubTitle
										className={cx("paragraphSubTitle", p.variant, variant, ...layoutClasses)}
									>
										{p.subHeading}
									</ContentSection.SubTitle>
								)}
								{p.content && (
									<ContentSection.Content
										content={p.content?.json}
										className={cx("paragraphContent", p.variant, variant, ...layoutClasses)}
									/>
								)}
							</ContentSection>
						))}
					</ContentSection.Content>
				)}
			</ContentSection>
			{footer && (
				<RichText
					data={footer.json}
					className={cx("sectionFooter", variant, ...layoutClasses)}
					listClassName={cx("list")}
				/>
			)}
		</>
	);
};
ParagraphSection.propTypes = {
	footer: object,
	variant: oneOf(["primary", "wedding"]),
	heading: string,
	subHeading: string,
	paragraphs: arrayOf(object),
	layout: arrayOf(string),
};
ParagraphSection.defaultProps = {
	footer: null,
	variant: null,
	heading: null,
	subHeading: null,
	paragraphs: null,
	layout: null,
};
export default ParagraphSection;
export { ParagraphSection };

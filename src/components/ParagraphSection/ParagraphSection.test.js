import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { ParagraphSection } from "./ParagraphSection";

describe("ParagraphSections", () => {
	test("renders with weddingPriceTag variant", () => {
		const { asFragment } = render(
			<ParagraphSection
				heading="Welcome to Sunwing Weddings"
				variant="primary"
				paragraphs={[
					{
						id: "75545bf4-e7b9-5087-80a8-07565b4dff16",
						heading: "Wedding consulting - ",
						subHeading: null,
						variant: "inline",
						content: {
							json: {
								data: {},
								content: [
									{
										data: {},
										content: [
											{
												data: {},
												marks: [],
												value:
													"Five hours of personalized destination wedding consulting services to help you with your overall vision and guide you in the right direction, including wedding planning tips and advice.\n\n",
												nodeType: "text",
											},
										],
										nodeType: "paragraph",
									},
								],
								nodeType: "document",
							},
						},
					},
				]}
				footer={{
					json: {
						nodeType: "document",
						data: {},
						content: [
							{
								nodeType: "embedded-entry-block",
								content: [],
								data: {
									target: {
										sys: {
											space: {
												sys: {
													type: "Link",
													linkType: "Space",
													id: "h82kzjd39wa1",
													contentful_id: "h82kzjd39wa1",
												},
											},
											id: "c3DrWIQGLPCSyZmaKEvOlqU",
											type: "Entry",
											createdAt: "2020-11-20T18:27:56.941Z",
											updatedAt: "2020-11-20T18:45:23.251Z",
											environment: {
												sys: {
													id: "replatform",
													type: "Link",
													linkType: "Environment",
													contentful_id: "replatform",
												},
											},
											revision: 3,
											contentType: {
												sys: {
													type: "Link",
													linkType: "ContentType",
													id: "label",
													contentful_id: "label",
												},
											},
											contentful_id: "3DrWIQGLPCSyZmaKEvOlqU",
										},
										fields: {
											title: "Wedding diamond services price tag",
											content: {
												data: {},
												content: [
													{
														data: {},
														content: [
															{
																data: {},
																marks: [],
																value: "Package rate ",
																nodeType: "text",
															},
															{
																data: {},
																marks: [{ type: "bold" }],
																value: "$499",
																nodeType: "text",
															},
														],
														nodeType: "paragraph",
													},
												],
												nodeType: "document",
											},
											variant: "weddingPriceTag",
										},
									},
								},
							},
						],
					},
				}}
			/>
		);
		expect(asFragment()).toMatchSnapshot();
	});
});

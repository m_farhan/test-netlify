import React from "react";
import { object } from "prop-types";
// eslint-disable-next-line import/no-cycle
import { ParagraphSection } from "./ParagraphSection";

const ParagraphSectionConnector = ({ data }) => (
	<ParagraphSection
		heading={data.heading}
		subHeading={data.subHeading}
		paragraphs={
			data.paragraphs && [
				...data.paragraphs?.map(i => ({
					...i,
					content: {
						...i.content,
						json: i.content?.raw && JSON.parse(i.content?.raw),
					},
				})),
			]
		}
		variant={data.variant}
		footer={{
			...data.otherDetails,
			json: data.otherDetails?.raw && JSON.parse(data.otherDetails.raw),
		}}
		layout={data.layout}
	/>
);

ParagraphSectionConnector.propTypes = {
	data: object.isRequired,
};

export default ParagraphSectionConnector;
export { ParagraphSectionConnector };

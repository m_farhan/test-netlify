import React from "react";
import { arrayOf, object, shape, string } from "prop-types";
import { RichText as RichTextComponent } from "../../sharedComponents/RichText";
/*
Upgrade check list
Update Label sorites and test
Update Paragraph section stories and test
Add stories and test to rich text (shared component and sw component)
Review all the rich text fragments
*/
const RichText = ({ data, components, className, listClassName }) => {
	const renderComponents = id => {
		const fields = components.find(i => i.contentful_id === id);
		if (!fields) {
			return "";
		}
		/*
		switch (fields.__typename) {
			case "ContentfulCloudinaryAsset":
				return (
					<img src={CloudinaryImageAdapter(fields?.cloudinaryAsset?.[0])} alt={fields.title} />
				);
			case "ContentfulList":
				return (
					<List theme={fields.theme} listStyle={fields.listStyle}>
						{fields.entries.map(entry => (
							<List.Item key={`${fields.id}-${entry.replace(" ", "-")}`}>{entry}</List.Item>
						))}
					</List>
				);
			case "ContentfulComponentBlock":
				return <TableConnector fields={fields.configuration} />;
			case "ContentfulLink":
				return (
					<Link to={fields.url} target={fields.target}>
						{fields.linkText}
						{fields.iconName && <Icon name={fields.iconName} />}
					</Link>
				);
			case "ContentfulButton":
				return <Button to={fields.link} theme={fields.variant} label={fields.text} />;
			case "ContentfulLabel":
				return <Label content={JSON.parse(fields.content.raw)} variant={fields.variant} />;
			default:
				return <></>;
		}
		*/
		return <pre>{JSON.stringify({ data, components, className, listClassName }, null, 4)}</pre>;
	};
	return (
		<RichTextComponent
			data={data}
			className={className}
			listClassName={listClassName}
			renderComponents={renderComponents}
		/>
	);
};

RichText.propTypes = {
	className: string,
	data: object.isRequired,
	listClassName: string,
	components: arrayOf(
		shape({
			id: string,
			__typename: string,
			contentful_id: string,
		})
	),
};

RichText.defaultProps = {
	className: null,
	listClassName: null,
	components: [],
};

export default RichText;
export { RichText };

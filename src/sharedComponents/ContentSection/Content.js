import React from "react";
import { string, node, oneOfType, arrayOf, object, shape } from "prop-types";
import { RichText } from "../RichText/RichText";

const ContentSectionContent = ({ children, content, components, className }) =>
	content ? <RichText className={className} data={content} components={components} /> : children;

ContentSectionContent.propTypes = {
	children: oneOfType([arrayOf(node), node]),
	className: string,
	content: object,
	components: arrayOf(
		shape({
			id: string,
			__typename: string,
			contentful_id: string,
		})
	),
};
ContentSectionContent.defaultProps = {
	children: null,
	className: null,
	content: null,
	components: [],
};
export default ContentSectionContent;
export { ContentSectionContent };

import React from "react";
import { string, node, oneOfType, arrayOf } from "prop-types";
import { ContentSectionTitle as Title } from "./Title";
import { ContentSectionSubTitle as SubTitle } from "./SubTitle";
import { ContentSectionContent as Content } from "./Content";

const ContentSection = ({ children, className }) => <div className={className}>{children}</div>;

ContentSection.Title = Title;
ContentSection.SubTitle = SubTitle;
ContentSection.Content = Content;
ContentSection.propTypes = {
	children: oneOfType([arrayOf(node), node]).isRequired,
	className: string,
};
ContentSection.defaultProps = {
	className: "",
};
export default ContentSection;
export { ContentSection };

import React from "react";
import { string, node, oneOfType, arrayOf } from "prop-types";

const ContentSectionSubTitle = ({ children, className }) => (
	<div className={className}>{children}</div>
);

ContentSectionSubTitle.propTypes = {
	children: oneOfType([arrayOf(node), node]).isRequired,
	className: string,
};
ContentSectionSubTitle.defaultProps = {
	className: "",
};

export default ContentSectionSubTitle;
export { ContentSectionSubTitle };

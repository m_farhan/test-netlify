import React from "react";
import { string, node, oneOfType, arrayOf } from "prop-types";

const ContentSectionTitle = ({ children, className }) => (
	<div className={className}>{children}</div>
);

ContentSectionTitle.propTypes = {
	children: oneOfType([arrayOf(node), node]).isRequired,
	className: string,
};
ContentSectionTitle.defaultProps = {
	className: "",
};

export default ContentSectionTitle;
export { ContentSectionTitle };

import React from "react";
import { func, object, string } from "prop-types";
import { BLOCKS, INLINES } from "@contentful/rich-text-types";
import { documentToReactComponents } from "@contentful/rich-text-react-renderer";

const RichText = ({ data, className, listClassName, renderComponents }) => {
	const options = {
		renderNode: {
			[BLOCKS.PARAGRAPH]: (node, children) => <p>{children}</p>,
			[BLOCKS.UL_LIST]: (node, children) => <ul className={listClassName}>{children}</ul>,
			[BLOCKS.EMBEDDED_ENTRY]: node => (
				<p>{renderComponents ? renderComponents(node?.data?.target?.sys?.id) : ""}</p>
			),

			[INLINES.EMBEDDED_ENTRY]: node =>
				renderComponents ? renderComponents(node?.data?.target?.sys?.id) : "",
		},
		renderText: text => text.split("\n").flatMap((_text, i) => [i > 0 && <br />, _text]),
	};
	return <div className={className}>{documentToReactComponents(data, options)}</div>;
};

RichText.propTypes = {
	className: string,
	listClassName: string,
	data: object.isRequired,
	renderComponents: func,
};

RichText.defaultProps = {
	className: null,
	renderComponents: null,
	listClassName: null,
};
export default RichText;
export { RichText };

import React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import { Heading, HeadingSize, HeadingType } from "../../components/Headings/Heading";
import * as styles from "./Section.module.scss";

const SectionType = ["section", "header", "footer"];
const SectionTitleAlign = ["center", "left"];
const SectionTheme = ["light", "dark", "none"];
const SectionPadding = ["none", "default", "top", "bottom"];

const Section = ({
	theme,
	padding,
	children,
	bgrImg,
	full,
	as,
	title,
	titleAs,
	titleSize,
	titleAlignment,
	titleId,
	className,
}) => {
	const Tag = `${as}`;
	const classes = cx(styles.section, {
		[styles[`section--${theme}`]]: theme,
		[styles[`section--padding-${padding}`]]: padding,
		[styles["section--full"]]: full,
		[styles["section--bgr-image"]]: bgrImg,
		[className]: className,
	});
	return (
		<Tag className={classes} data-section-theme={theme}>
			{bgrImg && <div className={styles.section__bgrImg}>{bgrImg}</div>}
			<div className={styles.section__inner}>
				{!!title && (
					<Heading
						as={titleAs}
						size={titleSize}
						center={titleAlignment}
						id={titleId}
						className={styles.section__title}
					>
						{title}
					</Heading>
				)}
				<div className={styles.section__content}>{children}</div>
			</div>
		</Tag>
	);
};

Section.propTypes = {
	as: PropTypes.oneOf([...SectionType]),
	theme: PropTypes.oneOf([...SectionTheme]),
	padding: PropTypes.oneOf([...SectionPadding]),
	full: PropTypes.bool,
	title: PropTypes.string,
	titleAs: PropTypes.oneOf([...HeadingType]),
	titleSize: PropTypes.oneOf([...HeadingSize]),
	titleAlignment: PropTypes.oneOf([...SectionTitleAlign]),
	titleId: PropTypes.string,
	className: PropTypes.string,
	children: PropTypes.node,
	bgrImg: PropTypes.string,
};

Section.defaultProps = {
	as: "section",
	theme: "light",
	padding: "default",
	full: false,
	title: undefined,
	titleAs: "h2",
	titleAlignment: "center",
	titleSize: undefined,
	titleId: undefined,
	className: undefined,
	children: undefined,
	bgrImg: undefined,
};

export default Section;
export { Section, SectionTitleAlign, SectionTheme, SectionPadding };

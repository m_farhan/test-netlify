import React from "react";
import { render } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";

import { Section } from "./Section";
import { Row, Cell } from "../../components/Grid";
import { cloudinaryImageAdapter } from "../CloudinaryImageAdapter";
import { Img } from "../Images/Image/Img";

describe("Section", () => {
	test("renders with basic", () => {
		const { asFragment } = render(
			<Section title="Section: Dynamic" titleAs="h2" titleSize="h1">
				<div>
					<p>Some kind of popup where you do whatever you need</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
						incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
						exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
						dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
						mollit anim id est laborum.
					</p>
				</div>
			</Section>
		);
		expect(asFragment()).toMatchSnapshot();
	});
	test("renders as dark background", () => {
		const { asFragment } = render(
			<Section theme="dark" title="Section: Dark">
				<div>
					<p>Some kind of popup where you do whatever you need</p>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
						incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
						exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
						dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
						mollit anim id est laborum.
					</p>
				</div>
			</Section>
		);
		expect(asFragment()).toMatchSnapshot();
	});
	test("renders as multiple", () => {
		const { asFragment } = render(
			<>
				<Section title="Section: Dark" titleAs="h2" titleSize="h1">
					<div>
						<p>Some kind of popup where you do whatever you need</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
							exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
							dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
							mollit anim id est laborum.
						</p>
					</div>
				</Section>
				<Section theme="dark" title="Section: Dark" titleAs="h2" titleSize="h1">
					<div>
						<p>Some kind of popup where you do whatever you need</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
							exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
							dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
							mollit anim id est laborum.
						</p>
					</div>
				</Section>
				<Section theme="dark">
					<div>
						<p>
							<strong>No title:</strong> Some kind of popup where you do whatever you need
						</p>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
							exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
							dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
							mollit anim id est laborum.
						</p>
					</div>
				</Section>
				<Section title="Section" titleAs="h2" titleSize="h1">
					<div>
						<p>
							<strong>No title:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit,
							sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
							veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
							dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
							in culpa qui officia deserunt mollit anim id est laborum.
						</p>
					</div>
				</Section>
				<Section>
					<div>
						<h2 className="align-center">Title inside the content</h2>
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
							incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
							exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
							dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
							Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
							mollit anim id est laborum.
						</p>
					</div>
				</Section>
			</>
		);
		expect(asFragment()).toMatchSnapshot();
	});
	test("renders as image background", () => {
		const { asFragment } = render(
			<Section
				title="Section"
				padding="none"
				bgrImg={
					<Img
						fluid={{
							...cloudinaryImageAdapter({
								cloudinaryImage: {
									url:
										"http://assets.sunwingtravelgroup.com/image/upload/f_auto,q_auto/v1609268784/Dev/Test%20Gallery/COU_ARU_009_vaytbp.jpg",
									secure_url:
										"https://assets.sunwingtravelgroup.com/image/upload/f_auto,q_auto/v1609268784/Dev/Test%20Gallery/COU_ARU_009_vaytbp.jpg",
									width: 1024,
									height: 576,
									raw_transformation: "f_auto,q_auto",
									version: 1609268784,
									public_id: "Dev/Test Gallery/COU_ARU_009_vaytbp",
									format: "jpg",
								},
							}).fluid,
						}}
					/>
				}>
				<Row cols="2">
					<Cell>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
						incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
						exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure
						dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
						Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
						mollit anim id est laborum.
					</Cell>
				</Row>
			</Section>
		);
		expect(asFragment()).toMatchSnapshot();
	});
});

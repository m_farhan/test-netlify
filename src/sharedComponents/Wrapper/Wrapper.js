import React from "react";
import { string, node, oneOfType, arrayOf, objectOf, bool, number } from "prop-types";
import classNames from "classnames/bind";
import * as styles from "./Wrapper.module.scss";

const cx = classNames.bind(styles);

const Wrapper = ({ children, style, className, containerClassName, withMargin, padding }) => (
	<div className={classNames("wrapper", className)} style={style}>
		<div
			className={`${padding ? "wrapper__content" : "page__content"} ${cx(containerClassName, {
				withMargin,
			})}`}
		>
			{children}
		</div>
	</div>
);

Wrapper.propTypes = {
	children: oneOfType([arrayOf(node), node]).isRequired,
	/**
	 * Wrapper Class
	 */
	className: string,
	containerClassName: string,
	padding: bool,
	style: objectOf(oneOfType([string, number])),
	/**
	 * It adds Bootstrap container to the wrapper and it makes it responsive
	 */
	withMargin: bool,
};
Wrapper.defaultProps = {
	className: "",
	containerClassName: "",
	style: {},
	withMargin: false,
	padding: false,
};

export default Wrapper;
export { Wrapper };

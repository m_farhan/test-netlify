import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";

const Country = ({ data }) => (
	<>
		<pre>{JSON.stringify(data, null, 4)}</pre>
	</>
);

/* Declare page prop */
Country.propTypes = {
	data: PropTypes.object.isRequired,
};

export default Country;

/* Query page data */
export const pageQuery = graphql`
	query GetCountryPageData($id: String!) {
		contentfulCountry(id: { eq: $id }) {
			title
			displayTitle
		}
	}
`;

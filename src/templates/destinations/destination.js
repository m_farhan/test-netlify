import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";

const Destination = ({ data }) => (
	<>
		<pre>{JSON.stringify(data, null, 4)}</pre>
	</>
);

/* Declare page prop */
Destination.propTypes = {
	data: PropTypes.object.isRequired,
};

export default Destination;

/* Query page data */
export const pageQuery = graphql`
	query GetDestinationPageData($id: String!) {
		contentfulDestination(id: { eq: $id }) {
			destinationName
		}
	}
`;

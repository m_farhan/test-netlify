import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { FluidSections } from "../components/FluidSections";
import { Heading } from "../components/Headings/Heading";

const Fluid = ({ data }) => {
	/* Retrieve Page Data from the page query */
	const pageData = data.contentfulFluidLayout;

	return (
		<>
			<Heading as="h1">H1 Heading</Heading>
			<Heading as="h1" size="h1alt">
				H1 Alternative
			</Heading>
			<Heading as="h2">H2 Sub heading</Heading>
			<Heading as="h3">H3 Title</Heading>
			<Heading as="h4">H4 Sub-title</Heading>
			<Heading as="h5">H5 Sub-title</Heading>
			<Heading as="h3" size="base">
				Heading with base style
			</Heading>
			<FluidSections data={pageData.pageSections} />
		</>
	);
};

/* Declare page prop */
Fluid.propTypes = {
	data: PropTypes.object.isRequired,
};

export default Fluid;

/* Query page data */
export const pageQuery = graphql`
	query GetFluidPageData($id: String!) {
		contentfulFluidLayout(id: { eq: $id }) {
			title
			pageSections {
				...pageSections
			}
		}
	}
`;

import React from "react";
import PropTypes from "prop-types";
import { graphql } from "gatsby";
import { FluidSections } from "../components/FluidSections";

const Home = ({ data }) => {
	/* Retrieve Page Data from the page query */
	const pageData = data.contentfulFluidLayout;

	return <FluidSections data={pageData.pageSections} />;
};

/* Declare page prop */
Home.propTypes = {
	data: PropTypes.object.isRequired,
};

export default Home;

/* Query page data */
export const pageQuery = graphql`
	query GetHomePageData($id: String!) {
		contentfulFluidLayout(id: { eq: $id }) {
			title
			pageSections {
				...pageSections
			}
		}
	}
`;

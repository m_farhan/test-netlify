const accents = require("remove-accents");

/* eslint-disable camelcase */
const createDestinationMapping = destinations => {
	const destinationMapping = {};

	destinations.forEach(({ node }) => {
		const {
			id,
			contentful_id,
			node_locale,
			seo,
			urlMappingSlug: destinationSlug,
			destinationCountry: countryData,
			destinationCode,
			excursions,
			destinationName,
			destinationCountry,
			pageSections,
		} = node;

		const locale = node_locale.slice(0, 2);
		const countrySlug =
			countryData && countryData.urlMappingSlug && countryData.urlMappingSlug.toLowerCase();

		if (!countrySlug) {
			return;
		}

		if (!destinationMapping[locale]) {
			destinationMapping[locale] = new Map();
		}

		let destinationString = "";
		let countryString = "";
		let countryCode = "";
		let destinationCodeString = "";

		if (destinationCode) {
			destinationCodeString = destinationCode;
		}
		if (destinationName) {
			destinationString = destinationName;
		}

		if (destinationCountry && destinationCountry.title) {
			countryString = destinationCountry.title;
		}
		if (destinationCountry && destinationCountry.excursionCode) {
			countryCode = destinationCountry.excursionCode;
		}

		let excursionIDs = [];

		if (excursions && excursions.contentIDs) {
			excursionIDs = excursions.contentIDs;
		}

		const destination = {
			id,
			contentful_id,
			slug: destinationSlug,
			node_locale,
			seo,
			excursions: excursionIDs,
			destination: destinationString,
			country: countryString,
			countryCode,
			destinationCode: destinationCodeString,
			pageSections,
		};

		if (destinationMapping[locale].has(countrySlug)) {
			destinationMapping[locale].get(countrySlug).push(destination);
		} else {
			destinationMapping[locale].set(countrySlug, [destination]);
		}
	});

	return destinationMapping;
};

const addSEOSupportToURL = url => {
	const urlComponents = url.split("/");
	const newURLComponents = [];
	urlComponents.forEach(i => {
		let newURL = i;
		newURL = accents.remove(newURL);
		newURL = newURL.replace(/^\s+|\s+$/g, ""); // Trim leading/trailing spaces
		newURL = newURL.replace(/[-,;_=\s]+/g, "-"); // Convert spaces to hyphens
		newURL = newURL.replace(/[^-\w\s]/g, ""); // Remove unneeded characters
		newURL = newURL.toLowerCase(); // Convert to lowercase
		newURLComponents.push(newURL);
	});
	return newURLComponents.join("/");
};

const getHotelURLRaw = (lang, country, destination, hotel) =>
	`/${lang}/hotel${country && `/${country}`}${destination && `/${destination}`}/${hotel}`;
const getHotelURL = (lang, country, destination, hotel) =>
	addSEOSupportToURL(getHotelURLRaw(lang, country, destination, hotel));

const getThingsToDoDestinationURLRaw = (language, slug) =>
	`/${language}/${language === "fr" ? "activités" : "things-to-do"}/${slug}`;

const getThingsToDoDestinationURL = (slug, destination) =>
	addSEOSupportToURL(getThingsToDoDestinationURLRaw(slug, destination));
const getThingsToDoURLRaw = (language, slug, product) =>
	`${getThingsToDoDestinationURLRaw(language, slug)}/${product}`;
const getThingsToDoURL = (language, slug, product) =>
	addSEOSupportToURL(getThingsToDoURLRaw(language, slug, product));

module.exports = {
	createDestinationMapping,
	addSEOSupportToURL,
	getHotelURLRaw,
	getHotelURL,
	getThingsToDoDestinationURLRaw,
	getThingsToDoDestinationURL,
	getThingsToDoURLRaw,
	getThingsToDoURL,
};

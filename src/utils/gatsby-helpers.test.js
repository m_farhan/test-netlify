import "@testing-library/jest-dom/extend-expect";

const utils = require("./gatsby-helpers");

describe("addSEOSupportToURL convert the string correctly ", () => {
	test("with accents char", () => {
		expect(
			utils.addSEOSupportToURL(
				"en/new-hotel-promo/  how can YOU_ME, and you/me can convert to you-me; its Amélie's test...   "
			)
		).toBe("en/new-hotel-promo/how-can-you-me-and-you/me-can-convert-to-you-me-its-amelies-test");
	});
	test("with /=+,;~``\"' chars", () => {
		expect(
			utils.addSEOSupportToURL(
				"en/new-hotel-promo/  This new hotel visit = great fun & you will get this/that =+,;~``\"'  "
			)
		).toBe("en/new-hotel-promo/this-new-hotel-visit-great-fun--you-will-get-this/that--");
	});
	test("with simple URL", () => {
		expect(utils.addSEOSupportToURL("en/hotel/cheap-hotel-deals")).toBe(
			"en/hotel/cheap-hotel-deals"
		);
	});
});
